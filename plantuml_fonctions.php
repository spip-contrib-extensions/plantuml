<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
 return;
}

function plantuml_pre_propre($texte) {

 if (!$texte) { return $texte; }

 $GLOBALS['basePath'] = rtrim(lire_config('plantuml/uml_host', 'https://www.plantuml.com/plantuml'),"/").'/';

 $GLOBALS['format'] = lire_config('plantuml/uml_format', 'svg');

 $startuml="@startuml";
 $enduml="@enduml";

 $preg=",".$startuml.".*?($|".$enduml.".),isS";

 if (str_contains($texte, $startuml) && str_contains($texte, $enduml) && preg_match_all($preg, $texte, $matches, PREG_SET_ORDER)) {
    foreach ($matches as $regs) {
      $uml_url=$GLOBALS['basePath'].$GLOBALS['format']."/".encodep(cleanup($regs['0']));
      if($GLOBALS['format']==='txt'){
        $data = recuperer_url($uml_url);
        $texte=str_replace( $regs['0'], "<pre>".$data['page']."</pre>", $texte);
      }else{
        $texte=str_replace( $regs['0'], '<img src="'.$uml_url.'" title="'.$uml_url.'">', $texte);
      }
    }
  }
 return $texte;
}

// Comment passer avant TextWheel ?
function  cleanUp($texte){
      $clean = str_replace( '&gt;','>', supprimer_tags($texte));
      $clean = str_replace( '&nbsp;',' ',$clean );
      $clean = str_replace( '&lt;','<',$clean );
      $clean = str_replace( '&mdash;','--',$clean );
      $clean = str_replace( '&#8211;','--',$clean );
      $clean = str_replace( '&#8217;',"'",$clean );
      $clean = str_replace( '&#8220;','"',$clean );
      $clean = str_replace( '&#8221;','"',$clean );
      $remove_array = array( '<p>', '</p>', '<br />', '<br/>' );
      $clean = str_replace( $remove_array, '', $clean );
  return $clean;
}

function encodep($text) {
    $compressed = gzdeflate($text, 9);
    return encode64($compressed);
}

function encode6bit($b) {
    if ($b < 10) {
         return chr(48 + $b);
    }
    $b -= 10;
    if ($b < 26) {
         return chr(65 + $b);
    }
    $b -= 26;
    if ($b < 26) {
         return chr(97 + $b);
    }
    $b -= 26;
    if ($b == 0) {
         return '-';
    }
    if ($b == 1) {
         return '_';
    }
    return '?';
}

function append3bytes($b1, $b2, $b3) {
    $c1 = $b1 >> 2;
    $c2 = (($b1 & 0x3) << 4) | ($b2 >> 4);
    $c3 = (($b2 & 0xF) << 2) | ($b3 >> 6);
    $c4 = $b3 & 0x3F;
    $r = "";
    $r .= encode6bit($c1 & 0x3F);
    $r .= encode6bit($c2 & 0x3F);
    $r .= encode6bit($c3 & 0x3F);
    $r .= encode6bit($c4 & 0x3F);
    return $r;
}

function encode64($c) {
    $str = "";
    $len = strlen($c);
    for ($i = 0; $i < $len; $i+=3) {
           if ($i+2==$len) {
                 $str .= append3bytes(ord(substr($c, $i, 1)), ord(substr($c, $i+1, 1)), 0);
           } else if ($i+1==$len) {
                 $str .= append3bytes(ord(substr($c, $i, 1)), 0, 0);
           } else {
                 $str .= append3bytes(ord(substr($c, $i, 1)), ord(substr($c, $i+1, 1)),
                     ord(substr($c, $i+2, 1)));
           }
    }
    return $str;
}
