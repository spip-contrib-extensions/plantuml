# Plugin PlantUML pour spip

![](images/plantuml-128.png)

> :zap: Un plugin basé sur PlantUML-server pour aider à la création de diagrammes

## Principe

[PlantUML](http://plantuml.com/) est utilisé pour dessiner à l'aide d'un langage simple à lire pour un utilisateur :
 * diagrammes de séquence
 * diagrammes de cas d'utilisation
 * diagrammes de classes
 * diagrammes d'objet
 * diagrammes d'activité
 * diagrammes de composant
 * diagrammes de déploiement
 * diagrammes d'état
 * diagrammes de temps
 * ...et bien plus encore !

## Mise en œuvre

Il est possible d'utiliser le [service en ligne](http://www.plantuml.com/plantuml) ou installer [votre propre serveur](https://github.com/plantuml/plantuml-server) PlantUML.

## Exemple

Un diagramme PlantUML est entouré par *@startuml* et *@enduml*.

Ce code :
```
@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: another authentication Response
@enduml
```
S'affichera ainsi en SVG, PNG ou TXT :

![](images/alice-bob.svg)
