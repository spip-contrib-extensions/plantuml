<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

    'plantuml_description' => 'Parseur PlantUML pour spip...',
    'plantuml_slogan' => 'Des diagrammes, vite !',
);
?>
