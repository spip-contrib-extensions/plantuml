<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
  return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

  'cfg_titre_plantuml' => 'Paramètres',

  'label_uml_format' => 'uml_format de sortie',
  'label_uml_format_svg' => 'svg (recommandé)',
  'label_uml_format_png' => 'png',
  'label_uml_format_txt' => 'txt',
  'label_uml_host' => 'Adresse du serveur',
  'label_uml_host_url' => 'https://www.plantuml.com/plantuml',

  'titre_page_configurer_plantuml' => 'Configuration PlantUML'
);
